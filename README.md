# Production
https://luna-scorpio.propulsion-learn.ch/

# Project LUNA
Project README: GIF / Images, Purpose, Setup Guide, Endpoints

# Initial setup 

<strong>Project and Frontend</strong>

1. Install node: https://nodejs.org/en/download/
2. Update npm: $ sudo npm install -g npm@latest
3. Install Docker: https://docs.docker.com/get-docker/
4. Clone the repository: $ git clone git@gitlab.propulsion-home.ch:full-stack/batch-13-aug-2020/group-projects/scorpio/project-luna.git
5. Install all dependencies: $ cd frontend/ $ npm i 
6. Start the frontend react app: cd frontend/ $ npm start
7. Test if the react app is running correctly by accessing "http://localhost:3000/"

<strong>Backend and Docker</strong>

8. Build the docker image: $ docker build -t registry.gitlab.propulsion-home.ch/full-stack/batch-13-aug-2020/group-projects/scorpio/project-luna .
9. Setup PyCharm with a remote interpreter for the docker container:
* PyCharm settings > Build, Execution, Deployment > Docker > virtual path "/backend" and local path "C:\Users\MyUser\PycharmProjects\project-luna\backend"
* PyCharm settings > Project > Add Interpreter > Docker Compose > Service: "backend" + Python interpreter path "/opt/conda/envs/backend/bin/python"
* Make PyCharm Run/Debug configurations > 
    * Script path: "C:\Users\MyUser\PycharmProjects\project-luna\backend\manage.py"
    * Command: "runserver 0.0.0.0:8000"
    * Path mapping: "C:/Users/alex_/PycharmProjects/project-luna/backend=/backend"
* Make PyCharm Run/Debug configurations > Same as above with command "makemigrations"
* Make PyCharm Run/Debug configurations > Same as above with command "migrate"
10. Test if the server is running correctly by accessing "http://localhost:8000/admin/"

# Every time for local development
1. Checkout new local development branch $ git checkout -b development (delete local one first $ git branch -d name)
2. Pull latest development branch from gitlab $ git pull origin development
3. If you have unmerged changes merge them into your local development branch from your previous branch
* $ git merge your-branch-to-integrate
* Resolve conflicts locally
* commit merge (to save and quit the command line commit editor "vim" $ :wq)
4. Create your new branch $ git checkout -b your-branch-name (e.g., branch name: create-login, connect-serializer, ...)
5. Start remote interpreter (Docker containers) with the configured PyCharm command "runserver"
6. Start frontend react app $ npm start
7. Happy Hacking!
8. Add, commit and push your local branch $ git add . $ git commit -m "message" $ git push origin your-branch-name
9. Create a merge request to the remote <strong>development</strong> branch on gitlab

# Troubleshoot and manipulate running server
* Go into the running container: $ docker exec -ti container_id bash (docker ps for id)
* Makemigrations, migrate and runserver with $ python manage.py your_command
* Delete records or drop local DB if needed 

# Manifesto Team SCORPIO
1. We aspire to clean code (DRY, KISS, YAGNI, Consistency, Readability)
2. We code like it's for someone else (a lot of small files, maintainability)
3. We code device responsive (@media queries and % dimensions)
4. We follow the SCRUM framework (daily stand ups 9AM + 2PM, sprints, agile development)
5. We beat team CAPRICORN
6. We work hard play hard

7. We use styled components 
8. We use verbose names 
* Divs: Container, FormContainer, InputContainer
* Components: Home, CreateRestaurant, CommentSnippet
* Variables frontend: (camelCase) toggleSwitch, email, isAuthorized
* Variables backend: (snake_case) amount_of_likes, 
* Class variables backend: (PascalCase) IsAuthorized
* File names frontend: camelCase.js 
* File names backend: snake_case.py
9. We use 4 spaces / tab as indentation (PyCharm default)
10. We $ git push origin my-branch-name (choose feature-branch-names) create a new one every time
11. We do merge requests to the "development" branch
12. After DevOps merged -> Merge local from remote "origin/development" (don't forget to fetch)
13. We deploy on "master" from "development"


I have read the manifesto and committed myself to it, Alex.
I have read the manifesto and committed myself to it, Sami.
I have read the manifesto and committed myself to it, Tobi.

# Scope
1. Day deploy / db design pony
2. Day sign in / sign up (skip if not completed by 2day)
3. Day new post / restaurant
4. Day profile
5. Day presentation / deployed / testing

