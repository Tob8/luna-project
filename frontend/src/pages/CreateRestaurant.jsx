import React from 'react';
import CreateRestaurant from "../components/CreateRestaurant";
import Header from "../components/header";
import Footer from "../components/footer";



const CreateRestaurantPage = () => {
    return (
            <>
                <Header/>
                    <CreateRestaurant/>
                <Footer/>
            </>
    )
}

export default CreateRestaurantPage