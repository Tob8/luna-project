import React from 'react';
import Registration3 from "../components/Registration3";
import Header from "../components/header";
import Footer from "../components/footer";



const RegistrationPage3 = () => {
    return (
            <>
                <Header />
                    <Registration3 />
                <Footer />
            </>
    )
}

export default RegistrationPage3