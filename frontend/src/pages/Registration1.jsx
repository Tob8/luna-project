import React from 'react';
import Registration1 from "../components/Registration1";
import Header from "../components/header";
import Footer from "../components/footer";



const RegistrationPage1 = () => {
    return (
            <>
                <Header />
                    <Registration1 />
                <Footer />
            </>
    )
}

export default RegistrationPage1