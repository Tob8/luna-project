import React from 'react';
import Registration2 from "../components/Registration2";
import Header from "../components/header";
import Footer from "../components/footer";



const RegistrationPage2 = () => {
    return (
            <>
                <Header />
                    <Registration2 />
                <Footer />
            </>
    )
}

export default RegistrationPage2