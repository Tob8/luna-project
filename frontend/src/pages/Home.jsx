import React from 'react'
import Header from "../components/header";
import Footer from "../components/footer";
import {
    BottomContainer,
    HomeContainer, RestaurantFeed,
    Searchbar, SearchBarContainer,
    SearchButton,
    SearchContainer,
    Title,
    TitleContainer, Underline
} from "../style/Home";
import {RestaurantBox} from "../components/RestaurantBox";


export const Home = () => {

    return (
        <>
            <Header />
                <HomeContainer>
                    <SearchContainer>
                        <SearchBarContainer>
                            <Searchbar placeholder="Search..."></Searchbar>
                            <SearchButton>Search</SearchButton>
                        </SearchBarContainer>
                    </SearchContainer>
                    <BottomContainer>
                        <TitleContainer>
                            <Title>Best Rated Restaurants</Title>
                            <Underline></Underline>
                        </TitleContainer>
                        <RestaurantFeed>
                            <RestaurantBox />
                            <RestaurantBox />
                            <RestaurantBox />
                            <RestaurantBox />
                        </RestaurantFeed>
                    </BottomContainer>
                </HomeContainer>
            <Footer />
        </>
    )
}
