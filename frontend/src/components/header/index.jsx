import { connect } from 'react-redux'
import React, { useState } from 'react';
import styled from 'styled-components'
import {
    HeaderContainer, HeaderLeft, HeaderRight, HeaderLogo,
    NavigationButtons, HomeBtn, SearchBtn, ProfileBtn,
    SignupLoginButtons, SignupBtn, LoginBtn
} from "../../style/Header";


const Header = (props) => {
    return (
        <HeaderContainer>
            <HeaderLeft>
                <HeaderLogo />
            </HeaderLeft>
            <HeaderRight>
                <NavigationButtons>
                    <HomeBtn>Home</HomeBtn>
                    <SearchBtn>Search</SearchBtn>
                    <ProfileBtn>Profile</ProfileBtn>
                </NavigationButtons>
                <SignupLoginButtons>
                    <SignupBtn>Signup</SignupBtn>
                    <LoginBtn>Login</LoginBtn>
                </SignupLoginButtons>
            </HeaderRight>
        </HeaderContainer>
    )
}

export default connect()(Header);
