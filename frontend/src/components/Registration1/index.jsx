import React, { useState } from 'react';
import { fetchRegister1 } from '../../store/actions/register1Actions';
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
    RegistrationContainer, TitleContainer, FormContainer,
    RegistrationTitle,
    FormEmail, FormBtn
} from "../../style/Registration1";
import { StandardButton } from "../../style/Buttons";



const Registration1 = (props) => {
    const [email, setEmail] = useState('');

    const register = async (event) => {
        event.preventDefault();
        const result = await props.dispatch(fetchRegister1(email));
        console.log(result);
        props.history.push('/posts');
    }

    return (

        <RegistrationContainer>

            <TitleContainer>
                <RegistrationTitle>Registration</RegistrationTitle>
            </TitleContainer>

            <FormContainer onSubmit={register}>

                <FormEmail
                    type="email"
                    placeholder="E-Mail address"
                    value={email}
                    onChange={(event) => setEmail(event.currentTarget.value)}
                />

                <FormBtn type="submit">
                  Register
                </FormBtn>

            </FormContainer>

        </RegistrationContainer>

    );
}

export default connect()(Registration1);
