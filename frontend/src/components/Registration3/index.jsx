import React, { useState } from 'react';
import { fetchRegister3 } from '../../store/actions/register3Actions.jsx';
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
    RegistrationContainer, TitleContainer, FormContainer,
    RegistrationTitle, FormValidationCode, FormLocation,
    FormEmail, FormBtn, FormPassword, FormPasswordRepeat
} from "../../style/Registration3";
import { StandardButton } from "../../style/Buttons";



const Registration3 = (props) => {
    let [state, setState] = useState({
      email: "",
      validationCode: "",
      location: "",
      password: "",
      password_repeat: "",
    });

    const handleChangePassword = e =>
    setState({ ...state, [e.target.name]: e.target.value });


    const register = async (event) => {
        event.preventDefault();
        const result = await props.dispatch(fetchRegister3(state.email));
        console.log(result);
        props.history.push('/posts');
    }

    return (
        <RegistrationContainer>

            <TitleContainer>
                <RegistrationTitle>Verification</RegistrationTitle>
            </TitleContainer>

            <FormContainer
                // onSubmit={login}
            >

                <FormEmail
                    type="email"
                    placeholder="E-Mail address"
                    value={state.email}
                    onChange={(event) => setState(event.currentTarget.value)}
                />

                <FormValidationCode
                    type="text"
                    placeholder="Validation code"
                    value={state.validationCode}
                    onChange={(event) => setState(event.currentTarget.value)}
                />

                <FormLocation
                    type="text"
                    placeholder="Location"
                    value={state.location}
                    onChange={(event) => setState(event.currentTarget.value)}
                />

                <FormPassword
                    type="password"
                    placeholder="Password"
                    value={state.password}
                    onChange={handleChangePassword}
                />

                <FormPasswordRepeat
                    type="password"
                    placeholder="Password repeat"
                    value={state.password_repeat}
                    onChange={handleChangePassword}
                />

                <FormBtn type="submit">
                  Finish Registration
                </FormBtn>

            </FormContainer>

        </RegistrationContainer>
    );
};

export default connect()(Registration3);
