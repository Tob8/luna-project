import React, { useState } from 'react';
import { restaurantAction } from '../../store/actions/restaurantActions';
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
    CreateRestaurantContainer, TitleContainer, CreateTitle, FormContainer,
    FormBasicContainer, FormName, FormCategory, FormCountry,
    FormAddressContainer, FormStreet, FormCity, FormZip,
    FormContactContainer, FormWebsite, FormPhone, FormEmail,
    FormDetailsContainer, FormOpenHours, FormPriceLevel, FormImageUpload,
    FormBtn
} from "../../style/CreateRestaurant";
import { StandardButton } from "../../style/Buttons";
// import { theme } from "../../style/index"


const CreateRestaurant = props => {
    const [restaurantName, setRestaurantName] = useState('');
    const [restaurantCategory, setRestaurantCategory] = useState('');
    const [restaurantCountry, setRestaurantCountry] = useState('');
    const [restaurantStreet, setRestaurantStreet] = useState('');
    const [restaurantCity, setRestaurantCity] = useState('');
    const [restaurantZip, setRestaurantZip] = useState('');
    const [restaurantWebsite, setRestaurantWebsite] = useState('');
    const [restaurantPhone, setRestaurantPhone] = useState('');
    const [restaurantEmail, setRestaurantEmail] = useState('');
    const [openHours, setOpenHours] = useState('');
    const [restaurantPriceLevel, setRestaurantPriceLevel] = useState('');
    const [restaurantImageUpload, setRestaurantImageUpload] = useState('');

    // let [state, setState] = useState({
    //   restaurantName: "",
    //   restaurantCategory: "",
    //   restaurantCountry: "",
    //   restaurantStreet: "",
    //   restarantCity: "",
    //   restarantZip: "",
    //   restaurantWebsite: "",
    //   restaurantPhone: "",
    //   restaurantEmail: "",
    //   openHours: "",
    //   restaurantPriceLevel: "",
    //   restaurantImageUpload: ""
    // });

    // const handleChange = e =>
    //     setState({ ...state, [e.target.name]: e.target.value });

    const createHandler = async (event) => {
        event.preventDefault();
        const result = await props.dispatch(restaurantAction(
            restaurantName, restaurantCategory, restaurantCountry,
            restaurantStreet, restaurantCity, restaurantZip,
            restaurantWebsite, restaurantPhone, restaurantEmail,
            openHours, restaurantPriceLevel, restaurantImageUpload
            ));
        props.history.push('/search/restaurants');
    }

    return (
    <>
        <CreateRestaurantContainer>

            <TitleContainer>
                <CreateTitle>Create new restaurant</CreateTitle>
            </TitleContainer>

            <FormContainer
                // onSubmit={submit}
            >



                <FormBasicContainer label={"Basic"}>
                    <FormName
                        type="text"
                        required
                        value={restaurantName}
                        onChange={(event) => setRestaurantName(event.currentTarget.value)}
                        label={"Name *"}
                    />
                    <FormCategory
                        value={restaurantCategory}
                        required
                        onChange={(event) => setRestaurantCategory(event.currentTarget.value)}
                        label={"Category *"}
                    />
                    {/*    <select id="category" name="category">*/}
                    {/*        <option value="luxury">Volvo</option>*/}
                    {/*        <option value="traditionel">Saab</option>*/}
                    {/*        <option value="fancy">Fiat</option>*/}
                    {/*    </select>*/}
                    {/*</FormCategory>*/}
                    <FormCountry
                        value={restaurantCountry}
                        required
                        onChange={(event) => setRestaurantCountry(event.currentTarget.value)}
                        label={"Country *"}
                    />
                    {/*    <select id="country" name="country">*/}
                    {/*        <option value="switzerland">Switzerland</option>*/}
                    {/*        <option value="mars">Mars</option>*/}
                    {/*        <option value="moon">Moon</option>*/}
                    {/*    </select>*/}
                    {/*</FormCountry>*/}
                </FormBasicContainer>



                <FormAddressContainer label={"Address"}>
                    <FormStreet
                        type="text"
                        value={restaurantStreet}
                        required
                        onChange={(event) => setRestaurantStreet(event.currentTarget.value)}
                        label={"Street *"}
                    />
                    <FormCity
                        type="text"
                        value={restaurantCity}
                        required
                        onChange={(event) => setRestaurantCity(event.currentTarget.value)}
                        label={"City *"}
                    />
                    <FormZip
                        type="text"
                        value={restaurantZip}
                        onChange={(event) => setRestaurantZip(event.currentTarget.value)}
                        label={"Zip"}
                    />
                </FormAddressContainer>



                <FormContactContainer label={"Contact"}>
                    <FormWebsite
                        type="text"
                        value={restaurantWebsite}
                        onChange={(event) => setRestaurantWebsite(event.currentTarget.value)}
                        label={"Website"}
                    />
                    <FormPhone
                        type="tel"
                        pattern="[0-9]{3} [0-9]{3} [0-9]{2} [0-9]{2}"
                        placeholder="079 123 45 67"
                        value={restaurantPhone}
                        required
                        onChange={(event) => setRestaurantPhone(event.currentTarget.value)}
                        label={"Phone *"}
                    />
                    <FormEmail
                        type="email"
                        value={restaurantEmail}
                        onChange={(event) => setRestaurantEmail(event.currentTarget.value)}
                        label={"E-Mail"}
                    />
                </FormContactContainer>



                <FormDetailsContainer label={"Details"}>
                    <FormOpenHours
                        type="text"
                        required
                        value={openHours}
                        onChange={(event) => setOpenHours(event.currentTarget.value)}
                        label={"Opening hours *"}
                    />
                    <FormPriceLevel
                        value={restaurantPriceLevel}
                        onChange={(event) => setRestaurantPriceLevel(event.currentTarget.value)}
                        label={"Price level"}
                    />
                    {/*    <select id="priceLevel" name="priceLevel">*/}
                    {/*        <option value="high">High</option>*/}
                    {/*        <option value="average">Average</option>*/}
                    {/*        <option value="low">Low</option>*/}
                    {/*    </select>*/}
                    {/*</FormPriceLevel>*/}
                    <FormImageUpload
                        type="file"
                        value={restaurantImageUpload}
                        onChange={(event) => setRestaurantImageUpload(event.currentTarget.value)}
                        label={"Image"}
                    />
                </FormDetailsContainer>

                <FormBtn type="submit">
                  Create
                </FormBtn>

            </FormContainer>

        </CreateRestaurantContainer>
        </>
  );
}

export default connect()(CreateRestaurant);

