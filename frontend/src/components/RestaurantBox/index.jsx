import React from 'react'
import {
    Address, BottomContainer,
    InfoContainer, AmountReviews,
    Overline, RatingContainer,
    RestaurantBoxContainer,
    RestaurantName, RestaurantPicture,
    TopContainer
} from "../../style/RestaurantBox";
import {Stars} from "../Stars";
import restaurantDefault from '../../assets/images/restaurant_cafe.jpg'


export const RestaurantBox = ({restaurant}) => {

    console.log("in RestaurantBox ->", restaurant)
    return (
        <>
            <RestaurantBoxContainer>
                <TopContainer>
                    <Overline></Overline>
                    <InfoContainer>
                        <RestaurantName>{restaurant ? restaurant.name : "Default Name"}</RestaurantName>
                        <Address>{restaurant ? restaurant.city : "Default Address"}</Address>
                        <RatingContainer>
                            <Stars />
                            <AmountReviews>18</AmountReviews>
                        </RatingContainer>
                    </InfoContainer>
                </TopContainer>
                <BottomContainer>
                    <RestaurantPicture src={restaurant ? restaurant.image : restaurantDefault}/>
                </BottomContainer>
            </RestaurantBoxContainer>
        </>
    )
}
