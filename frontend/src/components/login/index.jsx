import React, { useState } from 'react';
import { fetchLogin } from '../../store/actions/loginActions.jsx';
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
    LoginContainer, TitleContainer, FormContainer,
    LoginTitle,
    FormUsername, FormPassword, FormBtn
} from "../../style/Login";
import { StandardButton } from "../../style/Buttons";
// import { theme } from "../../style/index"


const Login = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const handleChangePassword = (event) => {
        setPassword(event.currentTarget.value)
    }

    const loginHandler = async (event) => {
        event.preventDefault();
        const result = await props.dispatch(fetchLogin(email, password));
        props.history.push('/home');
    }

    return (

        <LoginContainer>

            <TitleContainer>
                <LoginTitle>Login</LoginTitle>
            </TitleContainer>

            <FormContainer onSubmit={loginHandler}>

                <FormUsername
                    type="email"
                    placeholder="E-Mail"
                    value={email}
                    onChange={(event) => setEmail(event.currentTarget.value)}
                />

                <FormPassword
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={handleChangePassword}
                />

                <FormBtn type="submit">
                  Login
                </FormBtn>

            </FormContainer>

      </LoginContainer>
  );
}

export default connect()(Login);
