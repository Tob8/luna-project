import React, { useState } from 'react';
import { fetchRegister1 } from '../../store/actions/register1Actions.jsx';
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
    Registration2Container,
    TitleContainer, Registration2Title,
    TextContainer,
} from "../../style/Registration2";
import { StandardButton } from "../../style/Buttons";



const Registration2 = (props) => {
    return (
        <Registration2Container>

            <TitleContainer>
                <Registration2Title>Registration</Registration2Title>
            </TitleContainer>

            <TextContainer>
                Thanks for your registration.<br/>
                Our hard working monkeys are preparing a digital<br/>
                message called E-Mail that will be sent to you soon.<br/>
                Since monkeys arent good in writing the message could<br/>
                end up in you junk folder. Our apologies for any<br/>
                inconvienience.
            </TextContainer>

      </Registration2Container>
  );
}

export default connect()(Registration2);
