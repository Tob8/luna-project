import React from 'react'
import {ReviewsFeedContainer} from "../../style/ReviewsFeed";
import {ReviewBox} from "../ReviewBox";


export const ReviewsFeed = ({reviews}) => {

    return (
        <>
           <ReviewsFeedContainer>
               <ReviewBox />
               <ReviewBox />
               <ReviewBox />
               <ReviewBox />
           </ReviewsFeedContainer>
        </>
    )
}
