import React from 'react'
import {
    BottomContainer,
    CommentContainer,
    CommentCounter,
    CommentTitle, LatestCommentsContainer, LatestCommentTitle,
    LatestReviewContainer,
    LikeContainer,
    LikeCounter,
    LikeTitle, MostRecentCommentator, MostRecentCommentContainer, MostRecentCommentContent,
    Name,
    Overline,
    ProfileHeaderContainer,
    ProfileNameContainer,
    ProfilePicture,
    ProfilePictureContainer,
    ReadMoreLink,
    RestaurantName,
    ReviewBoxContainer, ReviewTextContainer, ReviewTextContent,
    ScndMostRecentCommentator, ScndMostRecentCommentContainer, ScndMostRecentCommentContent,
    SocialContainer,
    TopContainer,
    TotalReviews
} from "../../style/ReviewBox";
import ProfilePictureDefault from '../../assets/images/profile_default.png'
import { ReactComponent as LikeIcon } from '../../assets/svgs/like.svg';


export const ReviewBox = () => {

    return (
        <>
            <ReviewBoxContainer>

                <TopContainer>
                    <Overline></Overline>
                    <ProfileHeaderContainer>
                        <ProfilePictureContainer>
                            <ProfilePicture src={ProfilePictureDefault} />
                        </ProfilePictureContainer>
                        <ProfileNameContainer>
                            <Name>Laurent H.</Name>
                            <TotalReviews>6 Reviews in Total</TotalReviews>
                        </ProfileNameContainer>
                    </ProfileHeaderContainer>
                </TopContainer>

                <BottomContainer>

                    <LatestReviewContainer>
                        <RestaurantName>Colinz Bar</RestaurantName>
                        <ReviewTextContainer>
                            <ReviewTextContent>Ugh. Don't waste your time. Pizza dough good, thin crust but ingredients so so.Ugh. Don't waste your time. Pizza dough good, thin crust but ingredients so so. Ugh. Don't waste your time. Pizza dough good, thin crust but ingredients so so. Side of mixed vegetables very oily and mainly bell...</ReviewTextContent>
                            <ReadMoreLink to={'/restaurants/23'}>read more</ReadMoreLink>
                        </ReviewTextContainer>
                    </LatestReviewContainer>

                    <SocialContainer>
                        <LikeContainer>
                            <LikeIcon />
                            <LikeTitle>Like</LikeTitle>
                            <LikeCounter>63</LikeCounter>
                        </LikeContainer>
                        <CommentContainer>
                            <CommentTitle>Comment</CommentTitle>
                            <CommentCounter>23</CommentCounter>
                        </CommentContainer>
                    </SocialContainer>

                    <LatestCommentsContainer>
                        <LatestCommentTitle>Latest comments</LatestCommentTitle>
                        <MostRecentCommentContainer>
                            <MostRecentCommentator>Colin Wirz</MostRecentCommentator>
                            <MostRecentCommentContent>Actually you have no taste!</MostRecentCommentContent>
                        </MostRecentCommentContainer>
                        <ScndMostRecentCommentContainer>
                            <ScndMostRecentCommentator>Laurent Meyer</ScndMostRecentCommentator>
                            <ScndMostRecentCommentContent>Sorry bro!</ScndMostRecentCommentContent>
                        </ScndMostRecentCommentContainer>
                    </LatestCommentsContainer>

                </BottomContainer>

            </ReviewBoxContainer>
        </>
    )
}
