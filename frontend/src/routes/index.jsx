import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
// import AuthComponent from '../HOC/AuthComponent';
import { Home } from '../pages/Home';
import LoginPage from '../pages/login';
import RegistrationPage1 from '../pages/Registration1';
import RegistrationPage2 from '../pages/Registration2';
import RegistrationPage3 from '../pages/Registration3';
import CreateRestaurant from '../pages/CreateRestaurant';
import {Restaurants} from "../pages/Restaurants";

export const Routes = () => {
    return <>
        <Router>
            <Switch>
                <Route path="/auth/login" component={LoginPage} exact />
                <Route path="/auth/signup" component={RegistrationPage1} exact />
                <Route path="/auth/signup-message" component={RegistrationPage2} exact />
                <Route path="/auth/signup-verification" component={RegistrationPage3} exact />
                <Route path='/search/*' component={Restaurants} exact />
                <Route path='/create-restaurant' component={CreateRestaurant} exact />
                {/*<Route path='/*' component={AuthComponent(Home)} exact />*/}
                <Route path='/home' component={Home} exact />
                <Route path='/*' component={Home} exact />
            </Switch>
        </Router>
    </>
}
