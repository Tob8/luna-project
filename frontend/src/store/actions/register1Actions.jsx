import baseUrl from "../../helpers/baseUrl";



export const fetchRegister1 = (email) => async (dispatch, getState) => {
    console.log(email)
    const myHeaders = new Headers({
        "content-type": "application/json",
    });

    const body = {
        "email": email,
    }

    const config = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(body)
    };

    const response = await fetch(`${baseUrl}registration/`, config);
    const data = await response.json();

    return response
    console.log('registered: ', data)
};

// export default fetchRegister1
