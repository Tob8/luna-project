import baseUrl from "../../helpers/baseUrl";




export const fetchRegister3 = props => async (dispatch, getState) => {
    const myHeaders = new Headers({
        "content-type": "application/json",
    });

    const body = {
        email: props.email,
        code: props.validation_code,
        username: props.username,
        location: props.location,
        password: props.password,
        password_repeat: props.password_repeat,
    }

    const config = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(body)
    };

    const response = await fetch(`${baseUrl}registration/validate/`, config);
    const data = await response.json();
    return response
}
