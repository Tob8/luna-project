import styled from 'styled-components'
import {theme} from './index'
import headerLogo from '../assets/images/logo-luna.png'



export const HeaderContainer = styled.div`
    display: flex;
    flex-direction: row;
    border-bottom: 2px solid ${theme.colorPrimaryGrey};
    justify-content: space-between;
    height: 73px;
`

export const HeaderLeft = styled.div`
    display: flex;
    padding-left: 30px;
    align-items: center;
`

export const HeaderRight = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
`

export const HeaderLogo = styled.div`
    background-image: url("${headerLogo}");
    height: 24px;
    width: 102px;
`

export const NavigationButtons = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    gap: 2vw;
    padding-right: 2vw;
`

export const HomeBtn = styled.div`
    display: flex;
    font-weight: bold;
    height: 71px;
    //width: 64px;
    left: 16px;
    font-family: Helvetica;
    font-size: 2rem;
    font-style: normal;
    font-weight: 400;
    line-height: 23px;
    letter-spacing: 0px;
    text-align: center;
    align-items: center;
    border-bottom: 3px solid ${theme.colorTextWhite};
    :hover {
      border-bottom: 3px solid ${theme.colorPrimaryOrange};
    }
    :active {
      border-bottom: 3px solid ${theme.colorPrimaryOrange};
      font-weight: 700;

    }
`

export const SearchBtn = styled.div`
    height: 71px;
    //width: 64px;
    left: 16px;
    font-family: Helvetica;
    font-size: 2rem;
    font-style: normal;
    font-weight: 400;
    line-height: 23px;
    letter-spacing: 0px;
    text-align: center;
    display: flex;
    align-items: center;
    border-bottom: 3px solid ${theme.colorTextWhite};
    :hover {
      border-bottom: 3px solid ${theme.colorPrimaryOrange};
    }
    :active {
      border-bottom: 3px solid ${theme.colorPrimaryOrange};
      font-weight: 700;
    }
`

export const ProfileBtn = styled.div`
    height: 71px;
    //width: 100%;
    left: 16px;
    font-family: Helvetica;
    font-size: 2rem;
    font-style: normal;
    font-weight: 400;
    line-height: 23px;
    letter-spacing: 0px;
    text-align: center;
    display: flex;
    align-items: center;
    border-bottom: 3px solid ${theme.colorTextWhite};
    :hover {
      border-bottom: 3px solid ${theme.colorPrimaryOrange};
    }
    :active {
      border-bottom: 3px solid ${theme.colorPrimaryOrange};
      font-weight: 700;
    }
`

export const SignupLoginButtons = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    padding-right: 30px;
    font-family: Helvetica;
    font-size: 1.6rem;
    font-style: normal;
    font-weight: 400;
    line-height: 18px;
    letter-spacing: 0px;
    text-align: center;
    text-transform: uppercase;  
`

export const SignupBtn = styled.div`
    margin-right: 1px;
    width: 100px;
    height: 41px;
    border: none;
    border-radius: 28px 0 0 28px;
    background-color: ${theme.colorPrimaryOrange};
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const LoginBtn = styled.div`
    width: 100px;
    height: 41px;
    border: none;
    border-radius: 0 28px 28px 0;
    background-color: ${theme.colorPrimaryOrange};
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const LogoutBtn = styled.div`
    width: 201px;
    height: 41px;
    border: none;
    border-radius: 28px;
    background-color: ${theme.colorPrimaryOrange};
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`

