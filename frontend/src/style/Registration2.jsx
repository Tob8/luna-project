import styled from 'styled-components';
import { theme } from './index';
import {StandardButton} from "./Buttons";



export const Registration2Container = styled.div`
    height: calc(100vh - 73px - 92px);
    //height: 100%;
    width: 100vw;
    background-color: ${theme.colorPrimaryBackgroundLightGrey};
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    overflow: auto;
`

export const TitleContainer = styled.div`
    width: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-top: 45px;
    padding-bottom: 16px;
    border-bottom: 3px solid ${theme.colorPrimaryOrange};
`

export const Registration2Title = styled.div`
    display: flex;
    justify-content: center;
    align-items: flex-start;
    font-family: Helvetica;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: 28px;
    letter-spacing: 0px;
    text-align: center;
    text-transform: uppercase;  
    color: ${theme.colorTitleBlack};
`

export const TextContainer = styled.p`
    padding-top: 45px;
    display: flex;
    justify-content: center;
    font-family: Helvetica;
    font-size: 2rem;
    font-style: normal;
    font-weight: 400;
    line-height: 23px;
    letter-spacing: 0px;
    text-align: center;
`
