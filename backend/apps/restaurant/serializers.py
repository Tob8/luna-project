from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.restaurant.models import Restaurant
from apps.user.serializers import UserSerializer

User = get_user_model()


class RestaurantMinimalSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Restaurant
        fields = '__all__'


class RestaurantSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Restaurant
        fields = '__all__'
