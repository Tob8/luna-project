from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated

from apps.review.models import Review
from apps.review.serializers import ReviewSerializer
from apps.review.permissions import IsUserOrReadOnly

from apps.restaurant.models import Restaurant


class ListCreateReview(ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    lookup_field = 'restaurant_id'
    permission_classes = [IsAuthenticated & IsUserOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user, restaurant_id=self.kwargs['restaurant_id'])


class ReviewListAPIView(ListAPIView):
    """
    GET:
    Returns all reviews of given restaurant id
    """

    def get_queryset(self):
        # TODO is there a more efficient, cleaner way?
        requested_restaurant = Restaurant.objects.get(id=self.kwargs["restaurant_id"])
        return Review.objects.filter(restaurant=requested_restaurant)

    serializer_class = ReviewSerializer
    lookup_url_kwarg = 'restaurant_id'
