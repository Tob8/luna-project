from django.urls import path

from apps.review.views import ListCreateReview

urlpatterns = [
    path('<int:restaurant_id>/', ListCreateReview.as_view()),
    # path('<int:restaurant_id>/', ReviewListAPIView.as_view()),
]
